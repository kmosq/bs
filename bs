#!/bin/sh

PWD=$(pwd)
[ "$BS_DIR" ] && cd $BS_DIR

[ "$1" = "init" ] && {
	mkdir -p .bs
	touch .bs/env .bs/cmdmap .bs/fsmap
}

[ -d .bs ] || {
	echo "Directory $(pwd) is not a bs repo"
	exit 1
}

[ -d .bscache ] || mkdir -p .bscache
[ -f .bscache/localenv ] || touch .bscache/localenv

. .bs/env
. .bscache/localenv

PROFILE_DIR=
[ $PROFILE ] && [ -d .bs/$PROFILE ] && PROFILE_DIR=.bs/$PROFILE
[ $PROFILE_DIR ] && [ -f $PROFILE_DIR/env ] && . $PROFILE_DIR/env

[ "$DEFAULT_CMD" ] || DEFAULT_CMD='mkdir -p $(dirname $TARGET) && ln -sf $SOURCE $TARGET'
[ "$TARGET_DIR" ] || TARGET_DIR=$HOME
export TARGET_DIR
export PROFILE
export PROFILE_DIR

GFIND=find

[ "$(uname -s)" = "FreeBSD" ] && {
	command -v gfind > /dev/null && GFIND=gfind || {
		echo "GNU Find not installed" && exit 2;
	}
}
command -v perl > /dev/null || { echo "Perl not installed" && exit 2; }

setenv() {
	[ -f $1 ] || { echo "setenv: envfile $1 not found" >&2 && return 1; }
	[ "$2" ] || { echo "setenv: no env name" >&2 && return 1; }
	
	tempenv=$(mktemp)

	grep -q "$2=" $1 && sed 's|^'$2'=.*$|'"$2='$3'|" $1 > $tempenv && cp $tempenv $1 || echo "$2='$3'" >> $1

	rm $tempenv

	[ "$4" = "export" ] && {
		grep -q "export $2" $1 || echo "export $2" >> $1
	}
}

setexportenv() {
	[ -f $1 ] || { echo "setenv: envfile $1 not found" >&2 && return 1; }
	[ "$2" ] || { echo "setenv: no env name" >&2 && return 1; }

	grep -q "export $2" $1 || echo "export $2" >> $1
}

mktrans() {
	MAPFILE=$1
	cat $MAPFILE \
	| envsubst \
	| sed -e 's|\.|\\.|g' \
	| awk '{split($0, a, " ~ "); print length(a[1]) " s|" a[1] "|" a[2] "| and print and next;";}' \
	| sort -nr \
	| perl -pe 's|^.*? ||'
}

sortcmd() {
	CMDFILE=$1
	cat $CMDFILE \
	| awk '{split($0, a, " ~ "); print length(a[1]) " " $0;}' \
	| sort -nr \
	| perl -pe 's|^.*? ||;s| ~ |\n|;'
}

mkdeploy() {
	SOURCES=$(mktemp)
	TARGETS=$(mktemp)
	TRANSLATOR=$(mktemp)
	COMMANDS=$(mktemp)
	TRANSLATE="perl -n"
	
	GLOBALFILTER=cat
	FILTER=cat

	[ -x .bs/filter ] && GLOBALFILTER=.bs/filter
	[ $PROFILE_DIR ] && [ -x $PROFILE_DIR/filter ] && FILTER=$PROFILE_DIR/filter


	$GFIND . -type f -printf "%P\n" | $GLOBALFILTER | $FILTER | grep -v '^\.' > $SOURCES

	[ -f "$PROFILE_DIR/fsmap" ] && mktrans "$PROFILE_DIR/fsmap" >> $TRANSLATOR
	mktrans .bs/fsmap >> $TRANSLATOR
	echo 'print "~~IGNORE~~\n";' >> $TRANSLATOR
	
	cat $SOURCES | $TRANSLATE $TRANSLATOR > $TARGETS
	[ -f "$PROFILE_DIR/cmdmap" ] && sortcmd "$PROFILE_DIR/cmdmap" >> $COMMANDS
	sortcmd .bs/cmdmap >> $COMMANDS

	findcmd() {
		cat $COMMANDS | while read -r SEARCH; do
			read -r COMMAND
			echo $1 | grep -q $(echo $SEARCH | sed 's|\.|\\\.|g') && echo $COMMAND && return 234
		done
		[ $? -ne 234 ] && echo $DEFAULT_CMD
	}
	

	paste -d "\n" $SOURCES $TARGETS | while read -r SOURCE; do
		read -r TARGET
		echo "$TARGET" | grep -q '^~~IGNORE~~' && continue
		CMD=$(findcmd $SOURCE)
		export SOURCE=$(realpath $SOURCE)
		export TARGET
		echo $CMD | envsubst
	done
}

[ "$1" = "build" ] && {
	mkdeploy > .bscache/deploy
}

[ "$1" = "deploy" ] && {
	[ -f .bscache/deploy ] && sh .bscache/deploy || echo "You need to run 'bs build' first" >&2
}

[ "$1" = "set" ] && setenv .bscache/localenv $2 $3
[ "$1" = "export" ] && setexportenv .bscache/localenv $2

cd $PWD
